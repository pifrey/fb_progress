# README #

### Summary ###

Command line utility (in python) to access FritzBox configuration and status and show progress bar indicating Bytes sent. Also showing devices known to FritzBox and if they are online/offline and report when their status changes.

(soon to come)

### Setup ###

* Requires Python 2.7 or newer.
* also requires the following python modules:
  TODO to be described

* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner: p.i.frey@web.de
